import org.junit.jupiter.api.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.*;

@Nested
public class CalculatorResourceTest{

    @Test
    public void testCalculate(){

        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300+30";
        assertEquals(430, calculatorResource.calculate(expression));

        expression = " 300-99-101";
        assertEquals(100, calculatorResource.calculate(expression));

        String equation = "4*5*3";
        assertEquals(60, calculatorResource.calculate(equation));

        equation = "200/10/2";
        assertEquals(10, calculatorResource.calculate(equation));

    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300+100";
        assertEquals(500, calculatorResource.sum(expression));

        expression = "300+99";
        assertEquals(399, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "100-90-5";
        assertEquals(5, calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String equation = "5*4*5";
        assertEquals(100, calculatorResource.multiplication(equation));

    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String equation = "100/20/5";
        assertEquals(1, calculatorResource.division(equation));

    }
}
